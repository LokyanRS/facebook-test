package com.lokyanrs.support;

public class LinkDoesNotExistException extends Exception {
    public LinkDoesNotExistException() {
        System.out.println("Could not open link");
    }
}
