package com.lokyanrs.support;

import org.openqa.selenium.WebDriver;

import java.net.HttpURLConnection;
import java.net.URL;

public final class LinkOpenHelper {
    public static void openIfLinkExists(WebDriver driver, String url) throws LinkDoesNotExistException {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("HEAD");
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                driver.get(url);
            } else {
                throw new LinkDoesNotExistException();
            }
        } catch (Exception e) {
            throw new LinkDoesNotExistException();
        }
    }

    // private-конструктор, для невозможности создания экземпляра вспомогательного класса
    private LinkOpenHelper() {
    }
}
