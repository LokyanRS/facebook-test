package com.lokyanrs.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FacebookProfilePage {
    private WebDriver driver;

    @FindBy(xpath = "//a[@title='Профиль']/span/span")
    private WebElement profileLink;

    @FindBy(xpath = "//span[@id='fb-timeline-cover-name']/a")
    private WebElement timelineName;

    public FacebookProfilePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step("Открыть страницу со своим профилем")
    public void clickProfileLink() {
        profileLink.click();
    }

    public String getProfileName() {
        return profileLink.getText();
    }

    public String getTimelineName() {
        return timelineName.getText();
    }
}
