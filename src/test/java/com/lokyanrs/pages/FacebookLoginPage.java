package com.lokyanrs.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FacebookLoginPage {

    private WebDriver driver;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "pass")
    private WebElement passwordField;

    @FindBy(id = "loginbutton")
    private WebElement loginButton;

    @FindBy(xpath = "//div[normalize-space(text()) ='Вы ввели неверный пароль.']/a[normalize-space(text())='Забыли пароль?']")
    private WebElement passwordErrorMessage;


    public FacebookLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step("Ввести email ({email})")
    public void inputEmail(String email) {
        emailField.click();
        emailField.clear();
        emailField.sendKeys(email);
    }

    @Step("Ввести пароль ({password})")
    public void inputPassword(String password) {
        passwordField.click();
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    @Step("Нажать на \"Вход\"")
    public void clickLoginButton() {
        loginButton.click();
    }

    @Step("Проверить сообщение о неверном пароле")
    public void checkPasswordErrorMessage() {
        passwordErrorMessage.isDisplayed();
    }
}
