package com.lokyanrs.tests;

import com.lokyanrs.pages.FacebookLoginPage;
import com.lokyanrs.pages.FacebookProfilePage;
import com.lokyanrs.support.LinkDoesNotExistException;
import com.lokyanrs.support.LinkOpenHelper;
import io.qameta.allure.Epic;
import io.qameta.allure.junit4.DisplayName;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class FacebookOpenProfileTest {
    private String login;
    private String password;
    private String firstName;
    private String lastName;

    private static WebDriver driver;
    private static FacebookLoginPage loginPage;
    private static FacebookProfilePage profilePage;

    public FacebookOpenProfileTest(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Parameterized.Parameters(name = "Profile: {0}")
    public static Iterable<Object[]> passwordsForTest() {
        return Arrays.asList(new Object[][]{
                {"lokyanrs@gmail.com", "123456", "Роберт", "Локян"},
                {"popovia@mail.ru", "123456", "Иван", "Попов"}
        });
    }

    @Before
    public void setUp() throws LinkDoesNotExistException {
        // Очищаем кэш
        InternetExplorerOptions ieOptions = new InternetExplorerOptions().destructivelyEnsureCleanSession();

        System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
        driver = new InternetExplorerDriver(ieOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LinkOpenHelper.openIfLinkExists(driver, "https://www.facebook.com/");
        loginPage = new FacebookLoginPage(driver);
        profilePage = new FacebookProfilePage(driver);


    }

    @Test
    @Epic("Вход в систему")
    @DisplayName("Проверка входа в систему и отображения имени профиля")
    public void openProfileTest() {
        loginPage.inputEmail(login);
        loginPage.inputPassword(password);
        loginPage.clickLoginButton();
        assertEquals(firstName, profilePage.getProfileName());
        profilePage.clickProfileLink();
        assertEquals(String.format("%s %s", firstName, lastName), profilePage.getTimelineName());
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
