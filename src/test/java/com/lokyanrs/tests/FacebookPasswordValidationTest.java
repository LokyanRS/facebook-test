package com.lokyanrs.tests;

import com.lokyanrs.pages.FacebookLoginPage;
import com.lokyanrs.support.LinkDoesNotExistException;
import com.lokyanrs.support.LinkOpenHelper;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@RunWith(Parameterized.class)
public class FacebookPasswordValidationTest {
    private String password;
    private static WebDriver driver;
    private static FacebookLoginPage loginPage;

    public FacebookPasswordValidationTest(String password) {
        this.password = password;
    }

    @Parameterized.Parameters(name = "Password: {0}")
    public static Iterable<Object> passwordsForTest() {
        return Arrays.asList(new Object[]{
                "Nbyqh", "21w2", "3"
        });
    }

    @BeforeClass
    public static void setUp() throws LinkDoesNotExistException {
        // Очищаем кэш
        InternetExplorerOptions ieOptions = new InternetExplorerOptions().destructivelyEnsureCleanSession();

        System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
        driver = new InternetExplorerDriver(ieOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LinkOpenHelper.openIfLinkExists(driver, "https://www.facebook.com/");
        loginPage = new FacebookLoginPage(driver);
    }

    @Test
    @Epic("Вход в систему")
    @DisplayName("Проверка валидации пароля")
    public void passwordValidationTest() {
        loginPage.inputEmail("lokyanrs@gmail.com");
        loginPage.inputPassword(password);
        loginPage.clickLoginButton();
        loginPage.checkPasswordErrorMessage();
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}
